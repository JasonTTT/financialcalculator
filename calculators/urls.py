from django.conf.urls import url, include
from . import views
from calculators.views import HomeView, InvestView, LoansView, AnnuityView, CompoundView, PresentValueView, MortgageView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name="home"),
    url(r'^loans/', LoansView.as_view(), name="loans"),
    url(r'^annuity/', AnnuityView.as_view(), name="annuity"),
    url(r'^compoundinterest/', CompoundView.as_view(), name="compoundinterest"),
    url(r'^mortgage/', MortgageView.as_view(), name="mortgage"),
    url(r'^presentvalue/', PresentValueView.as_view(), name="presentvalue"),
]